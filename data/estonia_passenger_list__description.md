1. https://www.kaggle.com/christianlillelund/passenger-list-for-the-estonia-ferry-disaster
2. 27 сентября 1994 года паром «Эстония» отправился в ночное плавание по Балтийскому морю из порта Таллин в Эстонии в Стокгольм. Он отправился в 19.00 с 989 пассажирами и экипажем, а также транспортными средствами, и должен был прибыть в док в 09.30 следующего утра. К сожалению, «Эстония» так и не прибыла.

Погода была обычно ненастной для этого времени года, но, как и все другие паромы, запланированные в этот день, «Эстония» отправилась в путь как обычно. Примерно в 01:00 раздался тревожный звук скрежета металла, но немедленный осмотр носового козырька не показал ничего плохого. Корабль внезапно занесло спустя 15, и вскоре сработала сигнализация, в том числе сигнализация спасательной шлюпки. Вскоре после этого «Эстония» резко повернула на правый борт. Те, кто достиг палубы, имели шанс выжить, но те, кто этого не сделал, были обречены, поскольку угловые коридоры превратились в смертельные ловушки. Был отправлен сигнал Первомая, но отключение электричества означало, что местоположение корабля было указано неточно. «Эстония» исчезла с экранов радаров отвечающих кораблей около 01:50.

Marietta прибыла на место происшествия в 02:12, а первый вертолет - в 03:05. Из 138 спасенных живыми один скончался позже в больнице.

Из 310 человек, вышедших на палубу, почти треть умерла от переохлаждения. Окончательное число погибших было шокирующе высоким - более 850 человек.

Официальное расследование показало, что отказ замков на носовом козырьке, который откололся под сильными волнами, вызвал затопление водой палубы корабля из-за чего он был быстро опрокинут. В отчете также отмечалось отсутствие действий, задержка подачи сигнала тревоги, отсутствие наведения с мостика и неспособность зажечь аварийные сигнальные ракеты.

Крушение «Эстонии» было самой страшной послевоенной морской катастрофой в Европе.

Подробнее: https://en.wikipedia.org/wiki/MS_Estonia
3. Интересные вещи для исследования данных:
    * Согласно имеющимся данным, у кого больше шансов пережить затопление?
    * Возраст - показатель выживания?
    * Пол - показатель выживаемости?
    * У экипажа на борту больше шансов выжить, чем у пассажиров?
4. Информация о признаках
    * Country - Country of origin -	
    * Firstname - Firstname of passenger -	
    * Lastname - Lastname of passenger - 	
    * Sex - Gender of passenger - M = Male, F = Female
    * Age - Age of passenger at the time of sinking - 
    * Category - The type of passenger - C = Crew, P = Passenger
    * Survived - Survival	0 = No, 1 = Yes
